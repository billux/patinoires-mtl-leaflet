"use strict";

/**
 * Create map.
 */

var map = L.map('map').setView([45.5614, -73.7677], 11);
var OSMLayer = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: 'Fond de carte : &copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors ; État des patinoires : &copy; <a href="http://donnees.ville.montreal.qc.ca/dataset/patinoires">Ville de Montréal</a>.'
});
OSMLayer.addTo(map);
L.control.scale({'imperial':false}).addTo(map);

//var overlays = {
//    'Patinoires': SkatingRinksOverlay,
//};


/**
 * Load and parse XML from Ville de Montréal
 */
var JSONRequest = $.ajax({ url: 'rinks.json',
    dataType: 'json',
});
var XMLRequest = $.ajax({ url: 'L29_PATINOIRE.xml',
    dataType: 'xml',
});
$.when(JSONRequest, XMLRequest).done(addSkatingRinksOnMap);

function addSkatingRinksOnMap(JSONResponse, XMLResponse) {
    var skatingRinksGeocoded = JSONResponse[0];
    var skatingRinksCondition = XMLResponse[0];
    var $xml = $(skatingRinksCondition)
    var skatingRinks = [];

    $xml.find('patinoire').each(function() {
        /**
         * Extract data from Ville de Montréal's XML feed.
         */
        var name = $(this).find('nom').text(),
            updateDate = $(this).find('arrondissement').find('date_maj').text(),
            boroughKey = $(this).find('arrondissement').find('cle').text(),
            conditions = {
            'open': $(this).find('ouvert').text(),
            'déblayée': $(this).find('deblaye').text(),
            'arrosée': $(this).find('arrose').text(),
            'resurfacée': $(this).find('resurface').text(),
            'condition': $(this).find('condition').text(),
            };

        /**
         * Build popup text with extracted data.
         */
        var popupText = '<h2>' + name + '</h2>';
        if (conditions['open'] === "1") {
            popupText += '<p>Patinoire ouverte :</p>';
            popupText += '<ul class="conditions-list">';
            $.each(conditions, function(key, value) {
                if (key !== 'condition' && key !== 'open') {
                    var mark = '';
                    if (value === "1") {
                        mark = '<i class="fa fa-check" style="color: #a5f484;"></i>';
                    }
                    else {
                        mark = '<i class="fa fa-times" style="color: #f4ad84;"></i>';
                    }
                    popupText += '<li>' + mark + ' ' + key + '</li>';
                }
            });
            popupText += '</ul><p>Condition générale : ' + conditions['condition'] + '</p>';
        }
        else {
            popupText += '<p>Patinoire fermée</p>';
        }
        popupText += '<i>Mis à jour le ' + updateDate + '</i>';

        /**
         * Color the marker depending on condition.
         */
        var markerColor = '';
        if (conditions['open'] !== '1') {
            markerColor = '#a6b1b5';
        }
        else if (conditions['condition'] === 'Excellente') {
            markerColor = '#b8eefc';
        }
        else if (conditions['condition'] === 'Bonne') {
            markerColor = '#a5d6e2';
        }
        else if (conditions['condition'] === 'Mauvaise') {
            markerColor = '#8fbbc6';
        }
        else {
            markerColor = '#7597a0';
        }

        /**
         * Pictogram from Font Awesome
         */
        const iceSkatingIcon = L.divIcon({
            //html: '<span class="ice-skating-icon" style="background-color: ' + markerColor + ';"><i class="fa fa-skating fa-2x ice-skating-pictogram" style="transform: rotate(-45deg)"</i></span>',
            html: '<span class="ice-skating-icon" style="background-color: ' + markerColor + ';"><i class="fa fa-skating fa-2x ice-skating-pictogram"></i></span>',
            //iconSize: [20, 20],
            iconAnchor: [0, 24],
            labelAnchor: [-6, 0],
            popupAnchor: [0, -36],
        });

        /**
         * Add marker with its popup on map.
         */
        var skatingRink = skatingRinksGeocoded.filter(function(object) {
            return object['cle'] === boroughKey;
        });
        if (skatingRink.length >= 1) {
            skatingRink = skatingRink[0]['patinoires'].filter(function(object) {
                        return object['nom'] === name;
            });
            if (skatingRink.length >= 1) {
                if (skatingRink[0].lat && skatingRink[0].lng) {
                    var marker = L.marker(
                        [skatingRink[0].lat, skatingRink[0].lng],
                        {
                            color: markerColor,
                            icon: iceSkatingIcon,
                        }).addTo(map);
                    marker.bindPopup(popupText);
                }
                else {
                    console.log('No coordinates found in JSON: ' + name);
                }
            }
            else {
                console.log('Could not find skating rink in JSON: ' + name + ' (borough key: ' + boroughKey + ')');
            }
        }
        else {
            console.log('Could not find borough key in JSON: ' + boroughKey);
        }
    });
}
