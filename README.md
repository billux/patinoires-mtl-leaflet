# patinoires-mtl-leaflet

The map is available here: https://univers-libre.net/patinoires-mtl

## Credits

  * This script is licensed under the terms of the GNU General Public License version 3. Copyright Romain Dessort.
  * The data related to skating rinks (except coordinates) are licensed under the terms of the Creative Commons Attribution 4.0 International license. Copyright [Ville de Montréal](http://donnees.ville.montreal.qc.ca/dataset/patinoires).
  * Coordinates of skating rinks come from [this project](https://gitlab.com/mudar-ca/PatinerMontreal-Android) which is licensed under the terms of the GNU General Public License version 3. Copyright Mudar Noufal.
  * Map layer is provided by OpenStreetMap. Copyright OpenStreetMap contributors. See [http://osm.org/copyright](http://osm.org/copyright) for license.
